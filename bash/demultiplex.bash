#!/bin/bash

SAMPLE=$1 # "SFA-115"
R1=$2 #"data/fastq/130311_SN1126_M_L001_SFA-115_R1.fastq.bz2"
R2=$3 #"data/fastq/130311_SN1126_M_L001_SFA-115_R2.fastq.bz2"

awk -v sample=$SAMPLE '
    $3==sample{
        print ">"$1"_"$2"\n"$5 > "tmp/barcodes.fasta"
        r1file = $1"_"$2"_R1.fastq.gz"
        print "outputs/fastq/"r1file,"outputs/fastq/round1-"r1file,"outputs/fastq/round2-"r1file > "tmp/pool_rounds.txt"
        r2file = $1"_"$2"_R2.fastq.gz"
        print "outputs/fastq/"r2file,"outputs/fastq/round1-"r2file,"outputs/fastq/round2-"r2file > "tmp/pool_rounds.txt"
    }
' data/fastq/samples.txt

cutadapt --cores=$SLURM_CPUS_PER_TASK \
    -g file:tmp/barcodes.fasta \
    -o outputs/fastq/round1-{name}_R1.fastq.gz \
    -p outputs/fastq/round1-{name}_R2.fastq.gz \
    $R1 $R2

cutadapt --cores=$SLURM_CPUS_PER_TASK \
    -g file:tmp/barcodes.fasta \
    -o outputs/fastq/round2-{name}_R2.fastq.gz \
    -p outputs/fastq/round2-{name}_R1.fastq.gz \
    outputs/fastq/round1-unknown_R2.fastq.gz outputs/fastq/round1-unknown_R1.fastq.gz

while read OUTPUT ROUND1 ROUND2; do
  cat $ROUND1 $ROUND2 > $OUTPUT
  rm -f $ROUND1 $ROUND2
done < tmp/pool_rounds.txt
