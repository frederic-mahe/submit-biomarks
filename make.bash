#!/bin/bash

# prepare contextual data
sbatch analysis/biosamples.bash

# demultiplex illumina sequencing data
sbatch --cpus-per-task 16 analysis/demultiplex_illumina.bash

