#!/bin/bash

module load cutadapt/3.1

ls data/fastq | \
    grep fastq.bz2 | \
    awk -F"_" '
        {
            sample = $5
            file = $0
            sub(/R[12].fastq.+$/,"",file)
            forward = "data/fastq/"file"R1.fastq.bz2"
            reverse = "data/fastq/"file"R2.fastq.bz2"
            print sample,forward,reverse
        }
    ' | \
    sort | \
    uniq > manifests/demultiplex.txt

while read SAMPLE R1 R2; do
  bash bash/demultiplex.bash $SAMPLE $R1 $R2
done < manifests/demultiplex.txt
